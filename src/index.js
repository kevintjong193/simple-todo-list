import React from 'react';
import ReactDOM from 'react-dom';
import './App.css';
import LoginPage from './LoginPage/LoginPage'
import RegisterPage from './RegisterPage/index'
import AuthenticationChecker from './helpers/AuthenticationChecker'
import ToDoPage from './TodoPage/index'
import '@fortawesome/react-fontawesome'
import 'bootstrap/dist/css/bootstrap.css';
import {
  BrowserRouter as Router,
  Switch,
  Route,
} from "react-router-dom";

const Routing  = (
  <Router>
    <div className="App">
      <Switch>
        <Route path="/" exact component={LoginPage} />
        <Route path="/register" exact component={RegisterPage} />
        <AuthenticationChecker>
          <Route path="/todo" exact component={ToDoPage} />
        </AuthenticationChecker>
      </Switch>
    </div>
  </Router>
)

ReactDOM.render(
  Routing,
  document.getElementById('root')
);
