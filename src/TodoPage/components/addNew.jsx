import React, { Component } from 'react'
import API from 'axios'
import { jwtToken } from '../../helpers/jwt'

class addNew extends Component {
  
  render() {
    const {
      title,
      priority,
      note,
      errorMessage,
    } = this.props
    return (
      <section className={this.props.formClass}>
        <div className="col-12">
          <div className="row">
            <div className="col-12 mb-3">
              <input type="text" className="form-control" name="title" value={title} onChange={this.props.handleChangeValue} placeholder="Title"/>
            </div>
            <div className="col-12 mb-3">
              <select className="custom-select" name="priority" id="priority" onChange={this.props.handleChangeValue} value={priority}>
                <option value="" disabled>Priority</option>
                <option value="1">1 (low priority)</option>
                <option value="2">2 (medium priority)</option>
                <option value="3">3 (high priority)</option>
              </select>
            </div>
            <div className="col-12 mb-4">
              <input type="text" className="form-control" name="note" value={note} onChange={this.props.handleChangeValue} placeholder="notes"/>
              <span className="text-danger">{ errorMessage }</span>
            </div>
            <div className="col-6">
              <button className="btn btn-success btn-block" onClick={this.props.handleSubmitData}>Submit</button>
            </div>
            <div className="col-6">
              <button className="btn btn-secondary btn-block" onClick={this.props.handleAddNew(false)}>Cancel</button>
            </div>
          </div>
        </div>
      </section>
    )
  }
}

export default addNew;