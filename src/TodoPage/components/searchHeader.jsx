import React, { Component, Fragment } from 'react'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { faSearch } from "@fortawesome/free-solid-svg-icons";

class searchHeader extends Component {
  render() {
    const {
      filter,
      query,
    } = this.props
    return (
      <Fragment>
        <div className="col-5 pb-3">
          <input type="text" name="query" value={query} onChange={this.props.handleChangeValue} className="form-control" placeholder="Search" />
        </div>
        <div className="col-5 pb-3">
          <select className="custom-select" name="filter" onChange={this.props.handleChangeValue} value={filter}>
            <option value="all">All</option>
            <option value="done">Done</option>
            <option value="undone">Undone</option>
          </select>
        </div>
        <div className="col-2 pb-3">
          <button className="btn btn-default">
            <FontAwesomeIcon onClick={this.props.handleSearchData} icon={faSearch}/>
          </button>
        </div>
      </Fragment>
    )
  }
}

export default searchHeader;