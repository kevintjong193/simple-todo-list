import React, { Component } from 'react'
import { map } from 'lodash'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { faTrash, faPencilAlt, faMinus } from "@fortawesome/free-solid-svg-icons";

class todoComponent extends Component {
  handleShowAction = (todo) => {
    const { showAction } = this.props
    if (showAction === '') {
      return
    }
    return (
      <span className={`action-badge ${showAction}`}>
        { showAction === 'edit' ?
          <FontAwesomeIcon onClick={this.props.handleEditData(todo)} icon={faPencilAlt} />
          :
          <FontAwesomeIcon onClick={this.props.handleDeleteData(todo.id)} icon={faMinus} />
        }
      </span>
    )
  }

  showBadge = (priority) => {
    let priorityLevel = ''
    if (priority === 1) {
      priorityLevel = 'low'
    } else if (priority === 2) {
      priorityLevel = 'medium'
    } else {
      priorityLevel = 'high'
    }
    return ( <span className={`badge badge-pill badge-${priorityLevel}`}>{ priority }</span> )
  }

  render() {
    const {
      list,
      showAction,
    } = this.props

    return(
      list.map((todo, index) => {
        return (
          <section className="todo-content" key={`todo-index-${index}`}>
            <div className="col-12">
              <div className="row no-gutters align-items-center h-100">
                { showAction !== '' &&
                  <div className="col-1">
                    { this.handleShowAction(todo) }
                  </div>
                }
                <div className="col-10">
                  <p className="todo-title mb-0">
                    { todo.title }
                    { this.showBadge(todo.priority) }
                  </p>
                  <p className="mb-0">{ todo.note }</p>
                </div>
                <div className={`text-right ${showAction !== '' ? 'col-1': 'col-2'}`}>
                  <div className="custom-control custom-switch">
                    <input type="checkbox" className="custom-control-input" value={todo.isDone} checked={todo.isDone}/>
                    <label className="custom-control-label" onClick={this.props.handleChangeStatus(index)}>{ todo.isDone }</label>
                  </div>
                </div>
              </div>
            </div>
          </section>
        )
      })
    )
  }
}

export default todoComponent;