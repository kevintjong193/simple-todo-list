import React, { Component, Fragment } from 'react'
import { jwtToken } from '../helpers/jwt'
import API from 'axios'
import { NavLink } from 'react-router-dom'
import { cloneDeep } from 'lodash'
import AddNew from './components/addNew'
import SearchHeader from './components/searchHeader'
import TodoComponent from './components/todoComponent'
import { defaultTodoData } from '../helpers/defaultConst'


class ToDoPage extends Component {
  constructor(props) {
    super(props)

    this.state = {
      list: [],
      ...defaultTodoData,
      isEdit: false,
      errorMessage: '',
      formClass: 'todo-content add-form ',
      buttonContainerClass: 'action-container ',
      query: '',
      filter: 'all',
      showAction: '',
      isEditOrDelete: false,
      
    }
  }

  componentDidMount() {
    this.getData()
  }

  getData() {
    const jwt = jwtToken()
    const {
      query,
      filter
    } = this.state

    API.get('https://pomonatodo.herokuapp.com/todo/user', {
      headers: { Authorization: `${jwt}` },
      params: {
        q: query,
        filter: filter
      }
    }).then(response => {
      const data = response.data.data
      this.setState({
        list: data
      })
    }).catch(err => {
      console.log(err)
    })
  }

  handleSearchData = (e) => {
    this.getData()
  }

  handleChangeValue = (e) => {
    this.setState({
      [e.target.name]: e.target.value
    })
  }

  handleDeleteData = (id) => () => {
    const jwt = jwtToken()
    API.delete(`https://pomonatodo.herokuapp.com/todo/${id}`, {
        headers: { 'Authorization': jwt }
      }).then(response => {
        this.getData()
      }).catch(err => {
        console.log(err)
      })
  }

  handleEditData = (todo) => () => {
    this.handleShowHide(true)
    if (todo.id) {
      this.setState({
        id: todo.id,
        note: todo.note,
        priority: todo.priority,
        title: todo.title,
        isEdit: true,
      })
    }
  }

  addNewTodo = (jwt, data, list) => {
    API.post('https://pomonatodo.herokuapp.com/todo', {
      ...data }, {
      headers: { 'Authorization': jwt }
    }).then(response => {
      this.setState({
        ...defaultTodoData,
        list: [...list, response.data.data]
      }, () => {
        this.handleShowHide(false)
      })
    }).catch(err => {
      this.setState({ errorMessage: err.response.data.data.message })
    })
  }

  editTodo = (id, jwt, data) => {
    API.put(`https://pomonatodo.herokuapp.com/todo/${id}`, {
      ...data }, {
      headers: { 'Authorization': jwt }
    }).then(response => {
      this.setState({
        ...defaultTodoData,
        isEdit: false,
      }, () => {
        this.getData()
        this.handleShowHide(false)
      })
    }).catch(err => {
      this.setState({ errorMessage: err.response.data.data.message })
    })
  }

  handleSubmitData = () => {
    const {
      title,
      priority,
      note,
      list,
      isEdit,
      id,
    } = this.state
    const jwt = jwtToken()
    const data = {
      title: title,
      priority: parseInt(priority),
      note: note
    }
    isEdit === false ?
      this.addNewTodo(jwt, data, list)
    :
      this.editTodo(id, jwt, data)
  }

  handleShowHide = (isAdd) => {
    let initialClass = 'todo-content add-form'
    let showButton = 'action-container '
    if (isAdd === true) {
      initialClass = `${initialClass} show`
      showButton = `${showButton} d-none`
    }
    this.setState({
      ...defaultTodoData,
      errorMessage: '',
      formClass: initialClass,
      buttonContainerClass: showButton
    })
  }

  showActionButton = (action) => (e) => {
    const { isEditOrDelete } = this.state
    this.setState({
      showAction: action,
      isEditOrDelete: !isEditOrDelete
    })
  }

  handleChangeStatus = (index) => (e) => {
    const jwt = jwtToken()
    let list = cloneDeep(this.state.list)
    if (index !== undefined) {
      list[index].isDone = !list[index].isDone
      API.put(`https://pomonatodo.herokuapp.com/todo/${list[index].id}`, {
        isDone: list[index].isDone }, {
        headers: { 'Authorization': jwt }
      }).then(() => {
        this.getData()
      })
    }
  }

  handleAddNew = (isAdd) => () => {
    this.handleShowHide(isAdd)
  }

  renderListData() {
    const { list, priority, note, title, formClass, query, filter, errorMessage, isDone, showAction, isEditOrDelete, buttonContainerClass } = this.state
    return (
      <div className="row todo-list pt-3">
        <SearchHeader
          filter={filter}
          query={query}
          handleChangeValue={this.handleChangeValue}
          handleSearchData={this.handleSearchData}
        />
        <div className="todo-container">
          <section className={buttonContainerClass}>
            <button className="btn btn-primary btn-block mb-3" onClick={this.handleAddNew(true)}>Add New</button>
            <div className="row mb-3">
              { !isEditOrDelete ?
                <Fragment>
                  <div className="col-6">
                    <span className="action-link" onClick={this.showActionButton('edit')}>Edit</span>
                  </div>
                  <div className="col-6 text-right">
                    <span className="action-link"  onClick={this.showActionButton('delete')}>Delete</span>
                  </div>
                </Fragment>
                :
                <div className="col-12">
                  <span className="action-link"  onClick={this.showActionButton('')}>Cancel</span>
                </div>
              }
            </div>
          </section>
          <AddNew
            formClass={formClass}
            title={title}
            priority={priority}
            note={note}
            isDone={isDone}
            errorMessage={errorMessage}
            handleChangeValue={this.handleChangeValue}
            handleSubmitData={this.handleSubmitData}
            handleAddNew={this.handleAddNew}
          />
          <TodoComponent
            list={list}
            showAction={showAction}
            handleEditData={this.handleEditData}
            handleChangeStatus={this.handleChangeStatus}
            handleDeleteData={this.handleDeleteData}
          />
        </div>
      </div>
    )
  }

  render() {
    return (
      <div className="container">
        { this.renderListData() }
      </div>
    )
  }
}

export default ToDoPage;