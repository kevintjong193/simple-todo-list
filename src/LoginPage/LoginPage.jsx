import React, { Component } from 'react';
import { authLogin } from '../connection'
import API from 'axios'
import {
  Link
} from "react-router-dom";

class LoginPage extends Component {
  constructor(props) {
    super(props);

    this.state = {
      email: '',
      password: '',
      errorMessage: '',
    }
  }

  handleSubmitLogin = (e) => {
    e.preventDefault()
    API.post('https://pomonatodo.herokuapp.com/auth/login', {
      email: this.state.email,
      password: this.state.password,
    }).then(response => {
      sessionStorage.setItem('jwt-token', response.data.data.token)
      this.props.history.push('/todo')
    }).catch(error => {
      this.setState({
        errorMessage: error.response.data.data.message
      })
    })
  }

  handleChangeValue = (e) => {
    this.setState({ [e.target.name]: e.target.value })
  }

  render() {
    const {
      email,
      password,
      errorMessage,
    } = this.state
    return (
      <div>
        <div className="container login-page">
          <div className="row">
            <div className="col-12 text-center">
              <p>Login Page</p>
            </div>
            <div className="col-12">
              <form onSubmit={this.handleSubmitLogin}>
                <div className="form-group">
                  <input type="text" name="email" onChange={this.handleChangeValue} placeholder="email" value={email} className="form form-control mb-2"/>
                </div>
                <div className="form-group">
                  <input type="password" name="password" onChange={this.handleChangeValue} placeholder="password" value={password} className="form form-control mb-2"/>
                </div>
                <span className="text-danger small d-block mb-3">{ errorMessage }</span>
                <div className="row mb-3">
                  <div className="col-12">
                    <button className="btn btn-primary btn-block">Login</button>
                  </div>
                </div>
                <div className="text-center">
                  <Link to="/register">
                    Don't have account, register
                  </Link>
                </div>
              </form>
            </div>
          </div>
        </div>
      </div>
    );
  }
}

export default LoginPage;
