import API from 'axios'

export const fetchData = () => (
  API.get('https://jsonplaceholder.typicode.com/posts')
)

export const registerAccount = async (params) => {
  await API.post('https://pomonatodo.herokuapp.com/auth/register', {
    name: params.name,
    email: params.email,
    password: params.password,
  })
}

export const authLogin = (params) => {
  API.post('https://pomonatodo.herokuapp.com/auth/login', {
    email: params.email,
    password: params.password,
  })
}