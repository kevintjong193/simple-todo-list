import React, { Component } from 'react';
import { fetchData, registerAccount } from '../connection'
import { Link } from 'react-router-dom'

class RegisterPage extends Component {
  constructor(props) {
    super(props)
    this.state = {
      name: '',
      email: '',
      password: '',
      errorMessage: '',
    }
  }

  handleRegisterAccount = () => {
    registerAccount(this.state)
    .then(response => {
      this.props.history.push('/')
    })
    .catch(error => {
      this.setState({
        errorMessage: error.response.data.data.message
      })
    })
  }

  updateInputValue = (params) => (event) => {
    this.setState({
      [params]: event.target.value
    })
  }

  render() {
    const {
      name,
      email,
      password,
    } = this.state
    return (
      <div>
        <div className="container register-page">
          <div className="row">
            <div className="col-12 text-center">
              <p>Register Page</p>
            </div>
            <div className="col-12">
              <form action="">
                <div className="form-group">
                  <label htmlFor="name">Name</label>
                  <input type="text" id="name" value={name} placeholder="name" onChange={this.updateInputValue('name')} className="form form-control"/>
                </div>
                <div className="form-group">
                  <label htmlFor="email">email</label>
                  <input type="text" id="email" value={email} placeholder="email" onChange={this.updateInputValue('email')} className="form form-control"/>
                </div>
                <div className="form-group">
                  <label htmlFor="password">Password</label>
                  <input type="password" id="password" value={password} placeholder="password" onChange={this.updateInputValue('password')} className="form form-control"/>
                  </div>
                <div className="row">
                  <div className="col-6">
                    <button type="button" onClick={this.handleRegisterAccount} className="btn btn-primary btn-block">Register</button>
                  </div>
                  <div className="col-6">
                    <Link className="btn btn-secondary btn-block" to="/">
                      Cancel
                    </Link>
                  </div>
                </div>
              </form>
            </div>
          </div>
        </div>
      </div>
    );
  }
}

export default RegisterPage;
