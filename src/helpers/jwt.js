export const jwtToken = () => {
  return sessionStorage.getItem('jwt-token')
}