export const defaultTodoData = {
  id: null,
  title: '',
  priority: '',
  note: '',
  isDone: false,
  isEdit: false,
}