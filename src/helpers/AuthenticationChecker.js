import React, { Component } from 'react'
import { jwtToken } from './jwt'
import { withRouter } from 'react-router-dom'

class AuthenticationChecker extends Component {
  constructor(props) {
    super(props);
  }

  componentDidMount() {
    const jwt = jwtToken()
    if(!jwt) {
      this.props.history.push('/')
    }
  }

  render() {
    return(
      <div>
        { this.props.children }
      </div>
    )
  }
}

export default withRouter(AuthenticationChecker);